<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    public function customer_groups(){
        return $this->belongsTo('App\CustomerGroup', 'customer_group_id');
    }

    public function deposits(){
        return $this->hasMany('App\Deposit');
    }

    public function sales_masters(){
        return $this->hasMany('App\SalesMaster');
    }

    public function getCustomerGroup($customer_group){
        if ($this->customer_groups()->where('name', $customer_group)->first()) {
            return true;
        }
        return false;
    }
}
