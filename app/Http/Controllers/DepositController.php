<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Customer as Customer;
use App\CustomerGroup as CustomerGroup;
use App\Deposit as Deposit;
use App\User as User;
use Auth;

class DepositController extends Controller
{
    public function index(){
        $deposits = Deposit::all();
        $from = "";
        $to = "";
        return view('adminlte::layouts.reporting.deposit', compact('deposits', 'from', 'to'));
    }

    public function store(Request $request){

        if(($request->datecheck) == 'datecheck') {
            $datenya =$request->datenya;
            $date = explode(' - ', $datenya);
            $from = $date[0];
            $to = $date[1];
            $deposits = Deposit::whereBetween('created_at', [$from, $to])->get();
            return view('adminlte::layouts.reporting.deposit', compact('deposits', 'from', 'to'));
        }
    }
}
