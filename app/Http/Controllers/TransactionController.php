<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Menu as Menu;
use App\Customer as Customer;
use App\SalesMaster as SalesMaster;
use App\SalesDetail as SalesDetail;
use App\Outlet as Outlet;
use App\User as User;
use App\Balance as Balance;
use Auth;
use Alert;
use Debugbar;

class TransactionController extends Controller
{
    public function index(){
        $user_id = Auth::user()->id;
        $yuser = User::where('id', $user_id)->first();
        $otlet = $yuser->outlets->pluck('id')->first();
        $menus = Menu::where('outlet_id', $otlet)->get();
        $saldo = "";
        $customers = "";
        $card_number = "";
        return view('adminlte::layouts.transaction.home', compact('menus','saldo', 'customers', 'card_number'));
    }

    public function store(Request $request){

        if(($request->submit) == 'check') {
                $customers = Customer::where('card_number', $request->card_id)->first();
                $user_id = Auth::user()->id;
                $yuser = User::where('id', $user_id)->first();
                $otlet = $yuser->outlets->pluck('id')->first();
                if ($customers == null) {
                    $menus = Menu::where('outlet_id', $otlet)->get();
                    $saldo = "";
                    $customers = "";
                    $card_number = "";
                    Alert::error('Please Fill Card Number...', 'Oops!');
                    return view('adminlte::layouts.transaction.home', compact('menus','saldo', 'customers', 'card_number'));
                } elseif($customers->status == 'Inactive'){
                    $menus = Menu::where('outlet_id', $otlet)->get();
                    $saldo = "";
                    $customers = "";
                    $card_number = "";
                    Alert::error('Customer Inactive...', 'Oops!');
                    return view('adminlte::layouts.transaction.home', compact('menus','saldo', 'customers', 'card_number'));
                } else {
                    $saldo = $customers->deposits->sum('saldo') - $customers->sales_masters->sum('total');
                    $card_number = $customers->card_number;
                    $user_id = Auth::user()->id;
                    $yuser = User::where('id', $user_id)->first();
                    $otlet = $yuser->outlets->pluck('id')->first();
                    $menus = Menu::where('outlet_id', $otlet)->get();
                    return view('adminlte::layouts.transaction.home', compact('saldo', 'menus', 'customers', 'card_number'));
                }
                
        } elseif(($request->submit) == 'pay') {

                $customers = Customer::where('card_number', $request->card_number)->first();
                $saldo = $customers->deposits->sum('saldo') - $customers->sales_masters->sum('total');
                $card_number = $customers->card_number;
                $user_id = Auth::user()->id;
                $yuser = User::where('id', $user_id)->first();
                $otlet = $yuser->outlets->pluck('id')->first();
                $menus = Menu::where('outlet_id', $otlet)->get();

                $sisa_saldo = $request->sisa_saldo;
                if ($sisa_saldo < 0) {
                    $menus = Menu::where('outlet_id', $otlet)->get();
                    $saldo = "";
                    $customers = "";
                    $card_number = "";
                    Alert::error('Saldo Minus...', 'Oops!');
                    return view('adminlte::layouts.transaction.home', compact('menus','saldo', 'customers', 'card_number'));
                }
                $total = collect($request->subtot)->sum();
                $sales_master = new SalesMaster();
                $sales_master->customer_id = $customers->id;
                $sales_master->user_id = Auth::user()->id;
                $sales_master->total = $total;
                $sales_master->save();
                $user_id = Auth::user()->id;
                $yuser = User::where('id', $user_id)->first();
                $otlet = $yuser->outlets->pluck('id')->first();
                $balance = new Balance();
                $balance->outlet_id = $otlet;
                $balance->user_id = $user_id;
                $balance->saldo = $total;
                $balance->save();


                $menu_id = $request->mid;
                foreach ($menu_id as $menu_ids) {
                    $cekQty = $request->qty[$menu_ids];
                    $user_id = Auth::user()->id;
                    $yuser = User::where('id', $user_id)->first();
                    $otlet = $yuser->outlets->pluck('id')->first();
                    $sales_detail = new SalesDetail();
                    $sales_detail->sales_master_id = $sales_master->id;
                    $sales_detail->menu_id = $menu_ids;
                    $sales_detail->quantity = $cekQty;
                    $sales_detail->outlet_id = $otlet;
                    if($cekQty > 0)
                    {
                        $sales_detail->save();
                    }
                }
                return redirect()->route('transaction.index')->with('alert-success', 'Data Has Been Updated');
        }

            // $sales_detail = new SalesDetail;
            // $sales_detail = $sales_master->id;
            // $sales_detail->save();
    }
}
