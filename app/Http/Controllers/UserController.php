<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User as User;
use App\Role;
use App\Outlet as Outlet;
class UserController extends Controller
{
    public function index(){
        // $customer = \App\Customer::find($id);
        $users = User::all();
        return view('adminlte::layouts.user.home', compact('users'));
    }

    public function edit($id){
        $users = User::findOrFail($id);
        $outlets = Outlet::all();
        // return edit view
        return view('adminlte::layouts.user.edit', compact('users', 'outlets'));
    }

    public function update(Request $request, $id){

        //validation
        $this->validate($request,[
            'username' => 'required',
            'fullname' => 'required',
        ]);

        // create new data
        $user = User::findOrFail($id);
        $user->username = $request->username;
        $user->fullname = $request->fullname;
        $user->save();
        $userFind = User::where('username', $request['username'])->first();
        $userFind->roles()->detach();
        if ($request['role_admin']) {
            $userFind->roles()->attach(Role::where('name', 'Admin')->first());
        }
        if ($request['role_cashier']) {
            $userFind->roles()->attach(Role::where('name', 'Cashier')->first());
        }
        if ($request['role_owner']) {
            $userFind->roles()->attach(Role::where('name', 'Owner')->first());
        }
        $user->outlets()->detach();
        $user->outlets()->attach($request->outlet_select);
        return redirect()->route('user.index')->with('alert-success', 'Data Has Been Updated');
    }

    public function create(){
        return view('adminlte::layouts.user.create');
    }

    public function store(Request $request){
        
        //validation
        $this->validate($request,[
            'username' => 'required|unique:users,username',
            'fullname' => 'required',
            'password' => 'required',
            'confirm_password' => 'required|same:password',
        ]);

        // create new data
        $user = new User;
        $user->username = $request->username;
        $user->fullname = $request->fullname;
        $user->password = bcrypt($request['password']);
        $user->save();
        return redirect()->route('user.index')->with('alert-success', 'Data Has Been Updated');
    }
}
