<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Outlet as Outlet;
use App\User as User;
use App\Balance as Balance;
use Auth;

class OutletController extends Controller
{
    public function index(){

        $outlets = Outlet::all();
        return view('adminlte::layouts.outlet.home', compact('outlets'));
    }

    public function create(){
        return view('adminlte::layouts.outlet.create');
    }

    public function store(Request $request){
        
        //validation
        $this->validate($request,[
            'name' => 'required|unique:outlets,name',
        ]);

        // create new data
        $outlet = new Outlet;
        $outlet->name = $request->name;
        $outlet->status = $request->status;
        $outlet->save();
        return redirect()->route('outlet.index')->with('alert-success', 'Data Has Been Updated');
    }

    public function edit($id){
        $outlets = Outlet::findOrFail($id);

        // return edit view
        return view('adminlte::layouts.outlet.edit', compact('outlets', 'users'));
    }

    public function update(Request $request, $id){

        // create new data
        $user_ids = Auth::user()->id;
        $user = User::findOrFail($user_ids);
        $outlets = Outlet::findOrFail($id);
        $outlets->name = $request->name;
        $outlets->status = $request->status;
        $outlets->save();

        if (($request->balance) !== null ) {
            $balance = new Balance;
            $balance->outlet_id = $outlets->id;
            $balance->user_id = $user->id;
            $balance->saldo = $request->balance_temp - $request->balance;
            $balance->save();
        }
        return redirect()->route('outlet.index')->with('alert-success', 'Data Has Been Updated');
    }
}
