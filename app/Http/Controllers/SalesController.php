<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Customer as Customer;
use App\CustomerGroup as CustomerGroup;
use App\Deposit as Deposit;
use App\User as User;
use App\Outlet as Outlet;
use App\Menu as Menu;
use Auth;
use App\SalesMaster as SalesMaster;
use App\SalesDetail as SalesDetail;

class SalesController extends Controller
{
    public function index(){
        $sales_masters = SalesMaster::all();
        $useeer = Auth::user();
        $user_role = $useeer->roles->pluck('name')->first();
        if ($user_role == 'Admin') {
            $sales_details = SalesDetail::all();
        } else {
            $user_id = Auth::user()->id;
            $yuser = User::where('id', $user_id)->first();
            $otlet = $yuser->outlets->pluck('id')->first();
            $sales_details = SalesDetail::where('outlet_id', $otlet)->get();
        }
        $from = "";
        $to = "";
        return view('adminlte::layouts.reporting.sales', compact('sales_masters', 'sales_details', 'from', 'to'));
    }

    public function store(Request $request){

        if(($request->datecheck) == 'datecheck') {
            $datenya =$request->datenya;
            $date = explode(' - ', $datenya);
            $from = $date[0];
            $to = $date[1];
            $user = Auth::user();
            $user_role = $user->roles->pluck('name')->first();
            if ($user_role == 'Admin') {
                $sales_details = SalesDetail::whereBetween('created_at', [$from, $to])->get();
            } else {
                $user_id = Auth::user()->id;
                $yuser = User::where('id', $user_id)->first();
                $otlet = $yuser->outlets->pluck('id')->first();
                $sales_details = SalesDetail::whereBetween('created_at', [$from, $to])->where('outlet_id', $otlet)->get();
            }
            return view('adminlte::layouts.reporting.sales', compact('from', 'to', 'sales_details'));
        }
    }
}
