<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Customer as Customer;
use App\CustomerGroup as CustomerGroup;
use App\Deposit as Deposit;
use App\User as User;
use Auth;
use Alert;

class CustomerController extends Controller
{
    public function index(){

        $customers = Customer::all();
        $deposits = Deposit::all();
        $uhuy = Auth::user()->username;
        return view('adminlte::layouts.customer.home', compact('customers', 'deposits', 'uhuy'));
    }

    public function edit($id){
        $customers = Customer::findOrFail($id);
        $deposits = Deposit::all();
        // return edit view
        return view('adminlte::layouts.customer.edit', compact('customers'));
    }

    public function update(Request $request, $id){

        // create new data
        $customers = Customer::findOrFail($id);
        $user_ids = Auth::user()->id;
        $user = User::findOrFail($user_ids);
        $customers->name = $request->name;
        $customers->email = $request->email;
        $customers->status = $request->status;
        $customers->customer_group_id = $request->customer_group;
        $customers->save();

        if (($request->saldo) !== null ) {
            $deposit = new Deposit;
            $deposit->customer_id = $customers->id;
            $deposit->user_id = $user->id;
            $deposit->saldo = $request->saldo + $request->saldo_temp;
            $deposit->save();
        } if(($request->saldo_min) !== null ){
            $saldo = $request->saldo_temp - $request->saldo_min;
            if ($saldo < 0) {
                Alert::error('Saldo Minussss...', 'Oops!');
                $customers = Customer::all();
                $deposits = Deposit::all();
                $uhuy = Auth::user()->username;
                return view('adminlte::layouts.customer.home', compact('customers', 'deposits', 'uhuy'));
            } else {
                $deposit = new Deposit;
                $deposit->customer_id = $customers->id;
                $deposit->user_id = $user->id;
                $deposit->saldo = $request->saldo_temp - $request->saldo_min;
                $deposit->save();
            }
            
        }



        return redirect()->route('customer.index')->with('alert-success', 'Data Has Been Updated');
    }

    public function create(){
        return view('adminlte::layouts.customer.create');
    }

    public function store(Request $request){
        
        //validation
        $this->validate($request,[
            'card_number' => 'required|unique:customers,card_number',
            'email' => 'unique:customers,email',
        ]);

        // create new data
        $customers = new Customer;
        $customers->card_number = $request->card_number;
        $customers->name = $request->name;
        $customers->email = $request->email;
        $customers->status = $request->status;
        $customers->customer_group_id = $request->customer_group;
        $customers->save();
        return redirect()->route('customer.index')->with('alert-success', 'Data Has Been Updated');
    }
}
