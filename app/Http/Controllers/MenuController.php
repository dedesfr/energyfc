<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Menu as Menu;
use App\Outlet as Outlet;
use Alert;
class MenuController extends Controller
{
    public function index(){
        $menus = Menu::all();
        return view('adminlte::layouts.menu.home', compact('menus'));
    }

    public function create(){
        $outlets = Outlet::all();
        return view('adminlte::layouts.menu.create', compact('outlets'));
    }

    public function store(Request $request){
        
        //validation
        $this->validate($request,[
            'name' => 'required|unique:menus,name',
            'price' => 'required',
            'outlet_select' => 'required',
        ]);

        // create new data
        $menu = new Menu;
        $menu->name = $request->name;
        $menu->price = $request->price;
        $menu->type = $request->type;
        $menu->status = $request->status;
        $menu->outlet_id = $request->outlet_select;
        $menu->save();
        Alert::success('Redirecting to menu...', 'Menu Created Successfully!');
        return redirect()->route('menu.index')->with('alert-success', 'Data Has Been Updated');
    }

    public function edit($id){
        $menus = Menu::findOrFail($id);
        $outlets = Outlet::all();
        // return edit view
        return view('adminlte::layouts.menu.edit', compact('menus', 'outlets'));
    }

    public function update(Request $request, $id){

        //validation
        $this->validate($request,[
            'name' => 'required',
            'price' => 'required',
        ]);

        // create new data
        $menus = Menu::findOrFail($id);
        $menus->name = $request->name;
        $menus->price = $request->price;
        $menus->type = $request->type;
        $menus->status = $request->status;
        $menus->outlet_id = $request->outlet_select;
        $menus->save();
        return redirect()->route('menu.index')->with('alert-success', 'Data Has Been Updated');
    }
}
