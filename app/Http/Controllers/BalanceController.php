<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Balance as Balance;
use App\User as User;
use Auth;

class BalanceController extends Controller
{
    public function index(){
        $useeer = Auth::user();
        $user_role = $useeer->roles->pluck('name')->first();
        if ($user_role == 'Admin') {
            $balances = Balance::all();
        } else {
            $user_id = Auth::user()->id;
            $yuser = User::where('id', $user_id)->first();
            $otlet = $yuser->outlets->pluck('id')->first();
            $balances = Balance::where('outlet_id', $otlet)->get();
        }
        $from = "";
        $to = "";
        return view('adminlte::layouts.reporting.balance', compact('balances', 'from', 'to'));
    }

    public function store(Request $request){

        if(($request->datecheck) == 'datecheck') {
            $datenya =$request->datenya;
            $date = explode(' - ', $datenya);
            $from = $date[0];
            $to = $date[1];
            $user = Auth::user();
            $user_role = $user->roles->pluck('name')->first();
            if ($user_role == 'Admin') {
                $balances = Balance::whereBetween('created_at', [$from, $to])->get();
            } else {
                $user_id = Auth::user()->id;
                $yuser = User::where('id', $user_id)->first();
                $otlet = $yuser->outlets->pluck('id')->first();
                $balances = Balance::whereBetween('created_at', [$from, $to])->where('outlet_id', $otlet)->get();
            }
            return view('adminlte::layouts.reporting.balance', compact('from', 'to', 'balances'));
        }
    }
}
