<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Employee as Employee;

class EmployeeController extends Controller
{
    public function index(){

        $employees = Employee::all();
        return view('adminlte::layouts.employee.home', compact('employees'));
    }

    public function edit($id){
        $users = User::findOrFail($id);
        // return edit view
        return view('adminlte::layouts.employee.edit', compact('employees'));
    }

    public function update(Request $request, $id){

        //validation
        $this->validate($request,[
            'username' => 'required',
            'fullname' => 'required',
            'password' => 'required',
            'confirm_password' => 'required|same:password',
        ]);

        // create new data
        $employees = Employee::all($id);
        $employees->username = $request->username;
        $employees->fullname = $request->fullname;
        $employees->password = bcrypt($request['password']);
        $employees->save();
        return redirect()->route('employee.index')->with('alert-success', 'Data Has Been Updated');
    }

    public function create(){
        return view('adminlte::layouts.employee.create');
    }

    public function store(Request $request){
        
        //validation
        $this->validate($request,[
            'card_number' => 'required|unique:employees,card_number',
            'name' => 'required',
            'email' => 'required|unique:employees,email',
            'status' => 'required',
        ]);

        // create new data
        $employees = new Employee;
        $employees->card_number = $request->card_number;
        $employees->name = $request->name;
        $employees->email = $request->email;
        $employees->save();
    }
}
