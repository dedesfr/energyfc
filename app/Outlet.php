<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Outlet extends Model
{
    public function users(){
        return $this->belongsToMany('App\User', 'user_outlet', 'outlet_id', 'user_id');
    }

    public function menus(){
        return $this->hasMany('App\Menu');
    }

    public function balances(){
        return $this->hasMany('App\Balance');
    }
}
