<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Deposit extends Model
{
    public function users(){
        return $this->belongsTo('App\User', 'user_id');
    }

    public function customers(){
        return $this->belongsTo('App\Customer', 'customer_id');
    }
}
