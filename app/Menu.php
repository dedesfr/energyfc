<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    public function outlets(){
        return $this->belongsTo('App\Outlet', 'outlet_id');
    }
}
