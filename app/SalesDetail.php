<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalesDetail extends Model
{
    public function sales_masters(){
        return $this->belongsTo('App\SalesMaster', 'sales_master_id');
    }

    public function menus(){
        return $this->belongsTo('App\Menu', 'menu_id');
    }

}
