<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Balance extends Model
{
    public function users(){
        return $this->belongsTo('App\User', 'user_id');
    }

    public function outlets(){
        return $this->belongsTo('App\Outlet', 'outlet_id');
    }
}
