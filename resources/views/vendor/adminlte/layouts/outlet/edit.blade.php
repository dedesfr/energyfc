@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
		{{ trans('adminlte_lang::message.outlet_edit') }}
    </h1>
</section>
	<br>
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
                <form action="{{route('outlet.update', $outlets->id)}}" method="post">
                    <input type="hidden" name="_method" value="PATCH">
                    {{csrf_field()}}
                    <div class="form-group {{ ($errors->has('name')) ? $errors->first('name') : '' }}">
                        <input type="text" name="name" class="form-control" placeholder="Enter name Here" value="{{$outlets->name}}">
                        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                    </div>
                    <div class="row">
                        <div class="col-md-5">
                            <div class="form-group {{ ($errors->has('balance_temp')) ? $errors->first('balance_temp') : '' }}">
                                <input type="text" name="balance_temp" class="form-control" disabled placeholder="Enter balance Here" value="{{$outlets->balances->sum('saldo')}}">
                                {!! $errors->first('balance_temp', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>
                        <span class="col-md-1"> - </span>
                        <div class="col-md-6">
                            <div class="form-group {{ ($errors->has('balance')) ? $errors->first('balance') : '' }}">
                                <input type="text" name="balance" class="form-control" placeholder="Enter balance Here">
                                {!! $errors->first('balance', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>
                    </div>
                    <div class="form-group {{ ($errors->has('status')) ? $errors->first('status') : '' }}">
                        <label for="status_select">Select Status:</label> 
                        <select name="status" class="form-control">
                            @if($outlets->status === 'Active')
                                <option value="Active">Active</optio>
                                <option value="Inactive">Inactive</option>
                            @else
                                <option value="Inactive">Inactive</option>
                                <option value="Active">Active</option>
                            @endif
                        </select>
                        {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
                    </div>
                    <div class="form-group">
                        <input type="submit" class="btn btn-primary" value="save">
                    </div>
                </form>
			</div>
		</div>
	</div>
@endsection
