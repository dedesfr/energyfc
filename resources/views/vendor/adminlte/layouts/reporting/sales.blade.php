@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ trans('adminlte_lang::message.energy_fc') }}
@endsection


@section('main-content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
		{{ trans('adminlte_lang::message.sales_report') }}
    </h1>
</section>
	<br>
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-4">
				<div class="box">
					<form action="{{route('sales.store')}}" method="post">
					{{csrf_field()}}
						<div class="box-body">
							<div class="form-group col-md-12">
								<label>Report berdasarkan tanggal :</label>
								<div class="input-group">
									<div class="input-group-addon">
										<i class="fa fa-calendar"></i>
									</div>
									<input type="text" class="form-control pull-right" name="datenya" id="daterange">
								</div>
							</div>
							<div class="box-footer">
								<a href="{{ url('sales') }}" class="btn btn-primary" >Reset</a>
								<button type="submit" class="btn btn-primary" name="datecheck" style="float:right" value="datecheck" id="datecheck">Submit</button> 
							</div>
						</div>
					</form>
				</div>
			</div>
			<!-- Default box -->
			<div class="col-xs-12">
				<div class="box">
					<div class="box-body">
						<table class="table table-bordered table-striped" id="reporting">
						<thead>
						<tr>
							<th>User</th>
							<th>Customer</th>
							<th>Card Number</th>
							<th>Type</th>
							<th>Name</th>
							<th>Price</th>
							<th>Quantity</th>
							<th>Total</th>
							<th>Date</th>
						</tr>					
						</thead>
							@foreach ($sales_details as $c)
								<tr>
									<td>{{ $c->sales_masters->users->username }}</td>
									<td>{{ $c->sales_masters->customers->name }}</td>
									<td>{{ $c->sales_masters->customers->card_number }}</td>
									<td>{{ $c->menus->type }}</td>
									<td>{{ $c->menus->name }}</td>
									<td>{{ $c->menus->price }}</td>
									<td>{{ $c->quantity }}</td>
									<td>{{ $c->sales_masters->total }}</td>
									<td>{{ $c->created_at->timezone('Asia/Jakarta') }}</td>
								</tr>
							@endforeach
						</table>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
			</div>
		</div>
	</div>
@endsection
