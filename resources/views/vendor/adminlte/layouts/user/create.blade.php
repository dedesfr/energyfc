@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
		{{ trans('adminlte_lang::message.user_add') }}
    </h1>
</section>
	<br>
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
                <form action="{{route('user.store')}}" method="post">
                    {{csrf_field()}}
                    <div class="form-group {{ ($errors->has('username')) ? $errors->first('username') : '' }}">
                        <input type="text" name="username" class="form-control" placeholder="Enter Username Here">
                        {!! $errors->first('username', '<p class="help-block">:message</p>') !!}
                    </div>
                    <div class="form-group {{ ($errors->has('fullname')) ? $errors->first('fullname') : '' }}">
                        <input type="text" name="fullname" class="form-control" placeholder="Enter Fullname Here">
                        {!! $errors->first('fullname', '<p class="help-block">:message</p>') !!}
                    </div>
                    <div class="form-group {{ ($errors->has('password')) ? $errors->first('password') : '' }}">
                        <input type="password" name="password" class="form-control" placeholder="Enter Password Here">
                        {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
                    </div>
                    <div class="form-group {{ ($errors->has('confirm_password')) ? $errors->first('confirm_password') : '' }}">
                        <input type="password" name="confirm_password" class="form-control" placeholder="Enter Confirm Password Here">
                        {!! $errors->first('confirm_password', '<p class="help-block">:message</p>') !!}
                    </div>

                    <div class="form-group">
                        <input type="submit" class="btn btn-primary" value="save">
                    </div>
                </form>
			</div>
		</div>
	</div>
@endsection
