@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ trans('adminlte_lang::message.energy_fc') }}
@endsection


@section('main-content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
		{{ trans('adminlte_lang::message.user') }}
    </h1>
</section>
	<br>
	<a href="{{route('user.create')}}" class="btn btn-info"> Create New User</a>
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">

				<!-- Default box -->
				<div class="box">
					<div class="box-body">
						<table class="table table-striped" id="users">
						<thead>
						<tr>
							<th>Username</th>
							<th>Fullname</th>
							<th>Roles</th>
							<th>Action</th>
						</tr>					
						</thead>
							@foreach ($users as $c)
								<tr>
									<td>{{ $c->username }}</td>
									<td>{{ $c->fullname }}</td>
									@if (null !== ($c->roles->first()))
										<td>{{$c->roles->pluck('name')->implode(' | ', ', ')}}</td>
									@else
										<td>ga ada</td>
									@endif
									<td><a href="{{route('user.edit', $c->id)}}" class="btn btn-primary fa fa-pencil fa-lg"></a></td>
								</tr>
							@endforeach
						</table>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->

			</div>
		</div>
	</div>
@endsection
