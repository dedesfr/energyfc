@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
		{{ trans('adminlte_lang::message.user_edit') }}
    </h1>
</section>
	<br>
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
                <form action="{{route('user.update', $users->id)}}" method="post">
                    <input type="hidden" name="_method" value="PATCH">
                    {{csrf_field()}}
                    <div class="form-group {{ ($errors->has('username')) ? $errors->first('username') : '' }}">
                        <label for="username">Username</label>
                        <input type="text" name="username" class="form-control" placeholder="Enter username Here" value="{{$users->username}}">
                        {!! $errors->first('username', '<p class="help-block">:message</p>') !!}
                    </div>
                    <div class="form-group {{ ($errors->has('fullname')) ? $errors->first('fullname') : '' }}">
                        <label for="fullname">Fullname</label>
                        <input type="text" name="fullname" class="form-control" placeholder="Enter fullname Here" value="{{$users->fullname}}">
                        {!! $errors->first('fullname', '<p class="help-block">:message</p>') !!}
                    </div>
                    <div class="form-group {{ ($errors->has('password')) ? $errors->first('password') : '' }}">
                        <label for="password">Password</label>
                        <input type="password" name="password" class="form-control" placeholder="Enter password Here" value="{{$users->password}}">
                        {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
                    </div>
                    <div class="form-group {{ ($errors->has('confirm_password')) ? $errors->first('confirm_password') : '' }}">
                        <label for="confirm_password">Confirm Password</label>
                        <input type="password" name="confirm_password" class="form-control" placeholder="Enter confirm_password Here" value="{{$users->password}}">
                        {!! $errors->first('confirm_password', '<p class="help-block">:message</p>') !!}
                    </div>
                    <button type="button" class="btn btn-info" data-toggle="collapse" data-target="#role_select">Roles</button>
                    <div id="role_select" class="collapse form-group {{ ($errors->has('role')) ? $errors->first('role') : '' }}">
                        <label for="admin" class="checkbox-inline">Admin</label> <input type="checkbox" {{$users->hasRole('Admin') ? 'checked' : '' }} name="role_admin">
                        <label for="cashier" class="checkbox-inline">Cashier</label> <input type="checkbox" {{$users->hasRole('Cashier') ? 'checked' : '' }} name="role_cashier">
                        <label for="owner" class="checkbox-inline">Owner</label> <input type="checkbox" {{$users->hasRole('Owner') ? 'checked' : '' }} name="role_owner">
                        {!! $errors->first('role', '<p class="help-block">:message</p>') !!}
                    </div>
                    
                    <button type="button" class="btn btn-info" data-toggle="collapse" data-target="#outlet_select">Outlet</button>
                    <div id="outlet_select" class="collapse form-group {{ ($errors->has('outlet_select')) ? $errors->first('outlet_select') : '' }}">
                        @foreach($outlets as $outlet)
                            <label class="radio-inline"><input type="radio" value="{{$outlet->id}}" name="outlet_select" {{ old('outlet_select', $users->outlets->pluck('name')->implode(' | ', ', ')) === $outlet->name ? 'checked' : '' }}>{{$outlet->name}}</label>
                        @endforeach
                        {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
                    </div>

                    <div class="form-group" style="padding-top: 30px;">
                        <input type="submit" class="btn btn-primary" value="save">
                    </div>
                </form>
			</div>
		</div>
	</div>
@endsection
