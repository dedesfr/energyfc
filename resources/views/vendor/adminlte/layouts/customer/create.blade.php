@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
		{{ trans('adminlte_lang::message.customer_add') }}
    </h1>
</section>
    <br>
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
                <form action="{{route('customer.store')}}" method="post">
                    {{csrf_field()}}
                    <div class="form-group {{ ($errors->has('card_number')) ? $errors->first('card_number') : '' }}">
                        <input type="number" name="card_number" class="form-control" placeholder="Enter Card Number Here">
                        {!! $errors->first('card_number', '<p class="help-block">:message</p>') !!}
                    </div>
                    <div class="form-group {{ ($errors->has('name')) ? $errors->first('name') : '' }}">
                        <input type="text" name="name" class="form-control" placeholder="Enter Name Here">
                        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                    </div>
                    <div class="form-group {{ ($errors->has('email')) ? $errors->first('email') : '' }}">
                        <input type="text" name="email" class="form-control" placeholder="Enter Email Here">
                        {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
                    </div>
                    <div class="form-group {{ ($errors->has('status')) ? $errors->first('status') : '' }}">
                        {{-- <input type="text" name="status" class="form-control" placeholder="Enter status Here" value="{{$customers->status}}"> --}}
                        <label for="status_select">Select Status:</label> 
                        <select name="status" class="form-control">
                            <option value="Active">Active</option>
                            <option value="Inactive">Inactive</option>
                        </select>
                        {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
                    </div>
                    <div class="form-group {{ ($errors->has('customer_group')) ? $errors->first('customer_group') : '' }}">
                        <label for="customer_group_select">Select Customer Group:</label> 
                        <select name="customer_group" class="form-control">
                            <option value="1">Internal</option>
                            <option value="2">External</option>
                        </select>
                        {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
                    </div>
                    <div class="form-group">
                        <input type="submit" class="btn btn-primary" value="save">
                    </div>
                </form>
			</div>
		</div>
	</div>
@endsection
