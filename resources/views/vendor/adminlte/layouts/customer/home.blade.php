@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ trans('adminlte_lang::message.energy_fc') }}
@endsection


@section('main-content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
		{{ trans('adminlte_lang::message.customer') }}
    </h1>
</section>
	{{-- <div id="dates" class="pull-right">
		<span id="hari"></span>
		<span id="bulan"></span>
		<span id="tahun"></span>
	</div> --}}
	<br>
	@if((Auth::user()->roles->pluck('name')->first()) == 'Admin')
		<a href="{{route('customer.create')}}" class="btn btn-info" style="margin-bottom:20px;"> Create New Customer</a>
	@endif
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-12">
				<!-- Default box -->
				<div class="box">
					<div class="box-body">
						<table class="table table-striped" id="users">
						<thead>
						<tr>
							<th>Card Number</th>
							<th>Name</th>
							<th>Email</th>
							<th>Status</th>
							<th>Customer Group</th>
							<th>Saldo</th>
							<th>Created At</th>
							<th>Updated At</th>
							@if((Auth::user()->roles->pluck('name')->first()) == 'Admin')
								<th>Action</th>
							@endif
						</tr>					
						</thead>
							@foreach ($customers as $c)
								<tr>
									<td>{{ $c->card_number}}</td>
									<td>{{ $c->name }}</td>
                                    <td>{{ $c->email }}</td>
									@if ( $c->status === 'Active')
										<td>{{ $c->status }}</td>
									@else
										<td> <span style="color: red;">{{ $c->status }}</span></td>
									@endif
                                    <td>{{ $c->customer_groups->name }}</td>
									@if (null !== ($c->deposits->first()))
										<td>{{$c->deposits->sum('saldo') - $c->sales_masters->sum('total')}}</td>
									@else
										<td>ga ada</td>
									@endif
                                    <td>{{ $c->created_at->timezone('Asia/Jakarta') }}</td>
                                    <td>{{ $c->updated_at->timezone('Asia/Jakarta') }}</td>
									@if((Auth::user()->roles->pluck('name')->first()) == 'Admin')
										<td><a href="{{route('customer.edit', $c->id)}}" class="btn btn-primary fa fa-pencil fa-lg"></a></td>
									@endif
								</tr>
							@endforeach
						</table>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->

			</div>
		</div>
	</div>
@endsection
