@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
		{{ trans('adminlte_lang::message.customer_edit') }}
    </h1>
</section>
	<br>
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
                <form action="{{route('customer.update', $customers->id)}}" method="post">
                    <input type="hidden" name="_method" value="PATCH">
                    {{csrf_field()}}
                    <div class="form-group {{ ($errors->has('card_number')) ? $errors->first('card_number') : '' }}">
                        <label for="username">Card Number:</label>
                        <input type="text" name="card_number" class="form-control" disabled placeholder="Enter card_number Here" value="{{$customers->card_number}}">
                        {!! $errors->first('card_number', '<p class="help-block">:message</p>') !!}
                    </div>
                    <div class="form-group {{ ($errors->has('name')) ? $errors->first('name') : '' }}">
                        <label for="username">Name:</label>
                        <input type="text" name="name" class="form-control" placeholder="Enter name Here" value="{{$customers->name}}">
                        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                    </div>
                    <div class="form-group {{ ($errors->has('email')) ? $errors->first('email') : '' }}">
                        <label for="username">Email:</label>
                        <input type="text" name="email" class="form-control" placeholder="Enter email Here" value="{{$customers->email}}">
                        {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
                    </div>
                    <div class="row">
                        <div class="col-md-5">
                            <label for="username">Saldo:</label>
                            <div class="form-group {{ ($errors->has('saldo_temp')) ? $errors->first('saldo_temp') : '' }}">
                                <input type="number" name="saldo_temp" class="form-control" disabled placeholder="Enter saldo Here" value="{{$customers->deposits->sum('saldo') - $customers->sales_masters->sum('total')}}">
                                {!! $errors->first('saldo_temp', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>
                        <span class="col-md-1"> + </span>
                        <div class="col-md-6">
                            <div class="form-group {{ ($errors->has('saldo')) ? $errors->first('saldo') : '' }}">
                                <input type="number" name="saldo" class="form-control" placeholder="Add Saldo">
                                {!! $errors->first('saldo', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-5" aria-hidden="true"></div>
                        <span class="col-md-1"> - </span>
                        <div class="col-md-6">
                            <div class="form-group {{ ($errors->has('saldo_min')) ? $errors->first('saldo_min') : '' }}">
                                <input type="number" name="saldo_min" class="form-control" placeholder="Take Saldo">
                                {!! $errors->first('saldo_min', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="form-group {{ ($errors->has('status')) ? $errors->first('status') : '' }}">
                        <label for="status_select">Select Status:</label> 
                        <select name="status" class="form-control">
                            @if($customers->status === 'Active')
                                <option value="Active">Active</optio>
                                <option value="Inactive">Inactive</option>
                            @else
                                <option value="Inactive">Inactive</option>
                                <option value="Active">Active</option>
                            @endif
                        </select>
                        {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
                    </div>
                    <div class="form-group {{ ($errors->has('customer_group')) ? $errors->first('customer_group') : '' }}">
                        <label for="customer_group_select">Select Customer Group:</label> 
                        <select name="customer_group" class="form-control">
                            @if($customers->customer_groups->name === 'Internal')
                                <option value="1">Internal</option>
                                <option value="2">External</option>
                            @else
                                <option value="2">External</option>
                                <option value="1">Internal</option>
                            @endif


                        </select>
                        {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
                    </div>
                    <div class="form-group">
                        <input type="submit" class="btn btn-primary" value="save">
                    </div>
                </form>
			</div>
		</div>
	</div>
@endsection
