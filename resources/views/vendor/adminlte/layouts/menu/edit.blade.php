@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
		{{ trans('adminlte_lang::message.menu_edit') }}
    </h1>
</section>
	<br>
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
                <form action="{{route('menu.update', $menus->id)}}" method="post">
                    <input type="hidden" name="_method" value="PATCH">
                    {{csrf_field()}}
                    <div class="form-group {{ ($errors->has('name')) ? $errors->first('name') : '' }}">
                        <input type="text" name="name" class="form-control" placeholder="Enter name Here" value="{{$menus->name}}">
                        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                    </div>
                    <div class="form-group {{ ($errors->has('price')) ? $errors->first('price') : '' }}">
                        <input type="number" name="price" class="form-control" placeholder="Enter price Here" value="{{$menus->price}}">
                        {!! $errors->first('price', '<p class="help-block">:message</p>') !!}
                    </div>
                    <div class="form-group {{ ($errors->has('type')) ? $errors->first('type') : '' }}">
                        <label for="type_select">Select Type:</label> 
                        <select name="type" class="form-control">
                            @if($menus->type === 'Food')
                                <option value="Food">Food</option>
                                <option value="Drink">Drink</option>
                            @else
                                <option value="Drink">Drink</option>
                                <option value="Food">Food</option>
                            @endif
                        </select>
                        {!! $errors->first('type', '<p class="help-block">:message</p>') !!}
                    </div>

                    <div class="form-group {{ ($errors->has('status')) ? $errors->first('status') : '' }}">
                        <label for="status_select">Select Status:</label> 
                        <select name="status" class="form-control">
                            @if($menus->status === 'Active')
                                <option value="Active">Active</optio>
                                <option value="Inactive">Inactive</option>
                            @else
                                <option value="Inactive">Inactive</option>
                                <option value="Active">Active</option>
                            @endif
                        </select>
                        {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
                    </div>

                    <button type="button" class="btn btn-info {{ ($errors->has('outlet_select')) ? $errors->first('outlet_select') : '' }}" data-toggle="collapse" data-target="#outlet_select">Outlet</button>
                    {!! $errors->first('outlet_select', '<p class="help-block">:message</p>') !!}
                    <div id="outlet_select" class="collapse form-group {{ ($errors->has('outlet_select')) ? $errors->first('outlet_select') : '' }}">
                        @foreach($outlets as $outlet)
                            <label class="radio-inline"><input type="radio" value="{{$outlet->id}}" name="outlet_select" {{ old('outlet_select', $menus->outlets->name) === $outlet->name ? 'checked' : '' }}>{{$outlet->name}}</label>
                        @endforeach
                    </div>

                    <div class="form-group" style="padding-top: 30px;">
                        <input type="submit" class="btn btn-primary" value="save">
                    </div>
                </form>
			</div>
		</div>
	</div>
@endsection
