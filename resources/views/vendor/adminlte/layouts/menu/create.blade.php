@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
		{{ trans('adminlte_lang::message.menu_add') }}
    </h1>
</section>
	<br>
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
                <form action="{{route('menu.store')}}" method="post">
                    {{csrf_field()}}
                    <div class="form-group {{ ($errors->has('name')) ? $errors->first('name') : '' }}">
                        <input type="text" name="name" class="form-control" placeholder="Enter name Here">
                        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                    </div>
                    <div class="form-group {{ ($errors->has('price')) ? $errors->first('price') : '' }}">
                        <input type="number" name="price" class="form-control" placeholder="Enter price Here">
                        {!! $errors->first('price', '<p class="help-block">:message</p>') !!}
                    </div>
                    <div class="form-group {{ ($errors->has('type')) ? $errors->first('type') : '' }}">
                        <label for="type_select">Select Type:</label> 
                        <select name="type" class="form-control">
                            <option value="Food">Food</option>
                            <option value="Drink">Drink</option>
                        </select>
                        {!! $errors->first('type', '<p class="help-block">:message</p>') !!}
                    </div>
                    <div class="form-group {{ ($errors->has('status')) ? $errors->first('status') : '' }}">
                        {{-- <input type="text" name="status" class="form-control" placeholder="Enter status Here" value="{{$customers->status}}"> --}}
                        <label for="status_select">Select Status:</label> 
                        <select name="status" class="form-control">
                            <option value="Active">Active</option>
                            <option value="Inactive">Inactive</option>
                        </select>
                        {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
                    </div>
                    <button type="button" class="btn btn-info {{ ($errors->has('outlet_select')) ? $errors->first('outlet_select') : '' }}" data-toggle="collapse" data-target="#outlet_select">Outlet</button>
                    {!! $errors->first('outlet_select', '<p class="help-block">:message</p>') !!}
                    <div id="outlet_select" class="collapse form-group {{ ($errors->has('outlet_select')) ? $errors->first('outlet_select') : '' }}">
                        @foreach($outlets as $outlet)
                            <label class="radio-inline"><input type="radio" value="{{$outlet->id}}" name="outlet_select">{{$outlet->name}}</label>
                        @endforeach
                    </div>
                    <div class="form-group" style="padding-top: 25px;">
                        <input type="submit" class="btn btn-primary" value="save">
                    </div>
                </form>
			</div>
		</div>
	</div>
@endsection
