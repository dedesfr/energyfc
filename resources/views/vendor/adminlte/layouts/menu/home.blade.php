@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ trans('adminlte_lang::message.energy_fc') }}
@endsection


@section('main-content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
		{{ trans('adminlte_lang::message.menu') }}
    </h1>
</section>
	<br>
	<a href="{{route('menu.create')}}" class="btn btn-info" style="margin-bottom:20px;"> Create New Menu</a>
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-12">
				<!-- Default box -->
				<div class="box">
					<div class="box-body">
						<table class="table table-striped" id="users">
						<thead>
						<tr>
							<th>Name</th>
							<th>Outlet</th>
							<th>Type</th>
							<th>Price</th>
							<th>Status</th>
							<th>Action</th>
						</tr>					
						</thead>
							@foreach ($menus as $c)
								<tr>
									<td>{{ $c->name }}</td>
									@if (null !== ($c->outlets->first()))
										<td>{{$c->outlets->name}}</td>
									@else
										<td>ga ada</td>
									@endif
									<td>{{ $c->type }}</td>
									<td>{{ $c->price }}</td>
									<td>{{ $c->status }}</td>
                                    <td><a href="{{route('menu.edit', $c->id)}}" class="btn btn-primary fa fa-pencil fa-lg"></a></td>
								</tr>
							@endforeach
						</table>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->

			</div>
		</div>
	</div>
@endsection
