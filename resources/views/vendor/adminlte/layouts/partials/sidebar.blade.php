<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <li class="header">{{ trans('adminlte_lang::message.header') }}</li>
            <!-- Optionally, you can add icons to the links -->
            @if(Auth::user()->hasRole('Admin') == true)
                <li class="{{ Request::path() == 'user' ? 'active' : '' }}" ><a href="{{ url('user') }}"><i class='fa fa-user'></i> <span>{{ trans('adminlte_lang::message.user') }}</span></a></li>
                <li class="{{ Request::path() == 'customer' ? 'active' : '' }}" ><a href="{{ url('customer') }}"><i class='fa fa-users'></i> <span>{{ trans('adminlte_lang::message.customer') }}</span></a></li>
                <li class="treeview">
                    <a href="#"><i class='fa fa-home'></i> <span>{{ trans('adminlte_lang::message.outlet') }}</span> <i class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">
                        <li class="{{ Request::path() == 'outlet' ? 'active' : '' }}" ><a href="{{ url('outlet') }}">{{ trans('adminlte_lang::message.outlet') }}</a></li>
                        <li class="{{ Request::path() == 'menu' ? 'active' : '' }}" ><a href="{{ url('menu') }}">{{ trans('adminlte_lang::message.menu') }}</a></li>
                    </ul>
                </li>
                <li class="treeview">
                    <a href="#"><i class='fa fa-book'></i> <span>{{ trans('adminlte_lang::message.report') }}</span> <i class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">
                        <li class="{{ Request::path() == 'deposit' ? 'active' : '' }}" ><a href="{{ url('deposit') }}">{{ trans('adminlte_lang::message.deposit') }}</a></li>
                        <li class="{{ Request::path() == 'sales' ? 'active' : '' }}" ><a href="{{ url('sales') }}">{{ trans('adminlte_lang::message.sales') }}</a></li>
                        <li class="{{ Request::path() == 'balance' ? 'active' : '' }}" ><a href="{{ url('balance') }}">{{ trans('adminlte_lang::message.balance') }}</a></li>
                    </ul>
                </li>
            @elseif(Auth::user()->hasRole('Owner') == true)
                <li class="{{ Request::path() == 'customer' ? 'active' : '' }}" ><a href="{{ url('customer') }}"><i class='fa fa-link'></i> <span>{{ trans('adminlte_lang::message.customer') }}</span></a></li>
                <li class="treeview">
                    <a href="#"><i class='fa fa-book'></i> <span>{{ trans('adminlte_lang::message.report') }}</span> <i class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">
                        <li class="{{ Request::path() == 'sales' ? 'active' : '' }}" ><a href="{{ url('sales') }}">{{ trans('adminlte_lang::message.sales') }}</a></li>
                        <li class="{{ Request::path() == 'balance' ? 'active' : '' }}" ><a href="{{ url('balance') }}">{{ trans('adminlte_lang::message.balance') }}</a></li>
                    </ul>
                </li>
                <li class="{{ Request::path() == 'transaction' ? 'active' : '' }}" ><a href="{{ url('transaction') }}"><i class='fa fa-shopping-basket'></i> <span>{{ trans('adminlte_lang::message.transaction') }}</span></a></li>
            @else
                <li class="{{ Request::path() == 'customer' ? 'active' : '' }}" ><a href="{{ url('customer') }}"><i class='fa fa-users'></i> <span>{{ trans('adminlte_lang::message.customer') }}</span></a></li>
                <li class="{{ Request::path() == 'transaction' ? 'active' : '' }}" ><a href="{{ url('transaction') }}"><i class='fa fa-shopping-basket'></i> <span>{{ trans('adminlte_lang::message.transaction') }}</span></a></li>   
            @endif
        </ul><!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>
