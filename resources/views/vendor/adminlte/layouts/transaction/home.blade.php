@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ trans('adminlte_lang::message.energy_fc') }}
@endsection


@section('main-content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
		{{ trans('adminlte_lang::message.transaction') }}
    </h1>
</section>
	<br>
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-12">
				<form action="{{route('transaction.store')}}" method="post" id="foo">
					{{csrf_field()}}
                    <div class="form-group">
                        <input type="text" name="card_id" id ="cardFilter" class="form-control" placeholder="Enter card_number Here">
                    </div>
					<input type="submit" class="btn btn-default" value="check" name="submit" id="check"/>

				<!-- Default box -->
				<div class="box">
					<div class="box-body">
						<table class="table table-striped">
							<thead>
								<tr>
									<th>Type</th>
									<th>Name</th>
									<th>Price</th>
									<th>Quantity</th>
									<th>Subtotal</th>
								</tr>
							</thead>
								@foreach ($menus as $c)
									<input type="hidden" name="mid[]" value="{{$c->id}}">
									<tr>
										<td>{{ $c->type }}</td>
										<td>{{ $c->name }}</td>
										<td style="width:10%"><input type="text" size="5" class="form-control calcprice" name="price[{{$c->id}}]" value="{{ $c->price }}" readonly tabindex="-1"></td>
										<td style="width:5%"><input type="text" size="1" class="form-control calcqty" name="qty[{{$c->id}}]" placeholder="0"></td>
										<td style="width:15%"><input type="text" size="5" class="form-control calcsubtot" name="subtot[{{$c->id}}]" readonly tabindex="-1"></td>
									</tr>
								@endforeach
							<tfoot>
							<tr>
								<th></th><th></th><th></th><th></th>
							</tr>
							<tfoot>
						</table>
					@if($saldo !== "")

						<div class="form-group" id="wew">
							<div class="col-md-3">
								<label for="saldo">Saldo Member:</label>
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-envelope"></i></span>
									<input type="text" class="form-control" name="saldo" id="saldo" value="{{ $saldo }}" readonly>
								</div>
								<div class="input-group">
									<input type="hidden" class="form-control" name="card_number" value="{{ $card_number }}" readonly>
								</div>
							</div>
							<div class="col-md-3">
								<label for="email">Total Biaya:</label>
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-envelope"></i></span>
									<input type="text" size="1" class="form-control" name="total" id="total" readonly tabindex="-1">
								</div>
							</div>
							<div class="col-md-3">
								<label for="email">Sisa Saldo Deposit:</label>
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-envelope"></i></span>
									<input type="text" class="form-control" name="sisa_saldo" id="sisa_saldo" readonly>
								</div>
							</div>
							<div class="clearfix"></div>
							<br>
								<input type="button" class="btn btn-default" value="Calculate" name="calc_saldo" id="calc_sal" />
								<input type="submit" class="btn btn-primary" value="pay" name="submit" id="pay">	
						</div>
					@endif
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
			</form>
			</div>
		</div>
	</div>
@endsection
