$(document).ready(function(){
    $("#cardFilter").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 110, 190, 118, 86]) !== -1 ||
            // Allow: Ctrl+A, Command+A
            (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
            // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
                // let it happen, don't do anything
                return;
        }
        // include 13 to block enter
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
    $('#calc_sal').click(function(){
        var 
        form = document.getElementById("foo"),
        hargai = form.getElementsByClassName("form-control calcprice"),
        qtyi = form.getElementsByClassName("form-control calcqty"),
        subtoti = form.getElementsByClassName("form-control calcsubtot"),
        harga,
        qty,
        subtot,
        result = {};

        var i, vharga, vqty, vsubtot,tot = 0; 

        for(i = 0; i < hargai.length; i++ ){
            harga = hargai[i];
            qty = qtyi[i];
            subtot = subtoti[i];
            vharga = harga.value;
            vqty = qty.value;
            vsubtot = subtot.value;
            
            if(vqty > 0){
                subtoti[i].value = vharga * vqty;
                tot += parseInt(subtoti[i].value);
            }
        }
        $( "#total" ).val( tot );

        var saldonya = $( "#saldo" ).val();
        var totalnya = $( "#total" ).val();
        var sisanya = saldonya - totalnya;
        $( "#sisa_saldo" ).val(sisanya);
    });
});