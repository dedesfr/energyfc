<?php
/*
 * Same configuration as Laravel 5.2 make auth:
 * See https://github.com/laravel/framework/blob/5.2/src/Illuminate/Auth/Console/stubs/make/routes.stub
 * but take into account we have to add 'web' middleware group here because Laravel by defaults add this middleware in
 * RouteServiceProvider
 */
Route::group(['middleware' => 'web'], function () {
    Route::auth();

    // ----------------------------- User ----------------------------
    // User Index
    Route::get('/user', [
        'uses' => 'UserController@index',
        'as' => 'user.index',
        'middleware' => 'roles',
        'roles' => ['Admin', 'Owner']
    ]);

    // User Create
    Route::get('/user/create', [
        'uses' => 'UserController@create',
        'as' => 'user.create',
        'middleware' => 'roles',
        'roles' => ['Admin']
    ]);

    // User Edit
    Route::get('/user/{user}/edit', [
        'uses' => 'UserController@edit',
        'as' => 'user.edit',
        'middleware' => 'roles',
        'roles' => ['Admin']
    ]);

    // User Update
    Route::patch('/user/{user}', [
        'uses' => 'UserController@update',
        'as' => 'user.update',
        'middleware' => 'roles',
        'roles' => ['Admin']
    ]);

    // User Store
    Route::post('/user', [
        'uses' => 'UserController@store',
        'as' => 'user.store',
        'middleware' => 'roles',
        'roles' => ['Admin']
    ]);

    // ----------------------------- Customer ----------------------------

    // Customer Index
    Route::get('/customer', [
        'uses' => 'CustomerController@index',
        'as' => 'customer.index',
        'middleware' => 'roles',
        'roles' => ['Admin', 'Cashier', 'Owner']
    ]);

    // Customer Create
    Route::get('/customer/create', [
        'uses' => 'customerController@create',
        'as' => 'customer.create',
        'middleware' => 'roles',
        'roles' => ['Admin']
    ]);

    // Customer Edit
    Route::get('/customer/{customer}/edit', [
        'uses' => 'customerController@edit',
        'as' => 'customer.edit',
        'middleware' => 'roles',
        'roles' => ['Admin']
    ]);

    // customer Update
    Route::patch('/customer/{customer}', [
        'uses' => 'customerController@update',
        'as' => 'customer.update',
        'middleware' => 'roles',
        'roles' => ['Admin']
    ]);

    // customer Store
    Route::post('/customer', [
        'uses' => 'customerController@store',
        'as' => 'customer.store',
        'middleware' => 'roles',
        'roles' => ['Admin']
    ]);

    // ----------------------------- Outlet ----------------------------

    // outlet Index
    Route::get('/outlet', [
        'uses' => 'OutletController@index',
        'as' => 'outlet.index',
        'middleware' => 'roles',
        'roles' => ['Admin']
    ]);

    // outlet Create
    Route::get('/outlet/create', [
        'uses' => 'OutletController@create',
        'as' => 'outlet.create',
        'middleware' => 'roles',
        'roles' => ['Admin']
    ]);

    // outlet Edit
    Route::get('/outlet/{outlet}/edit', [
        'uses' => 'OutletController@edit',
        'as' => 'outlet.edit',
        'middleware' => 'roles',
        'roles' => ['Admin']
    ]);

    // outlet Update
    Route::patch('/outlet/{outlet}', [
        'uses' => 'OutletController@update',
        'as' => 'outlet.update',
        'middleware' => 'roles',
        'roles' => ['Admin']
    ]);

    // outlet Store
    Route::post('/outlet', [
        'uses' => 'OutletController@store',
        'as' => 'outlet.store',
        'middleware' => 'roles',
        'roles' => ['Admin']
    ]);

    // ----------------------------- Menu ----------------------------

    // menu Index
    Route::get('/menu', [
        'uses' => 'MenuController@index',
        'as' => 'menu.index',
        'middleware' => 'roles',
        'roles' => ['Admin']
    ]);

    // menu Create
    Route::get('/menu/create', [
        'uses' => 'MenuController@create',
        'as' => 'menu.create',
        'middleware' => 'roles',
        'roles' => ['Admin']
    ]);

    // menu Edit
    Route::get('/menu/{menu}/edit', [
        'uses' => 'MenuController@edit',
        'as' => 'menu.edit',
        'middleware' => 'roles',
        'roles' => ['Admin']
    ]);

    // menu Update
    Route::patch('/menu/{menu}', [
        'uses' => 'MenuController@update',
        'as' => 'menu.update',
        'middleware' => 'roles',
        'roles' => ['Admin']
    ]);

    // menu Store
    Route::post('/menu', [
        'uses' => 'MenuController@store',
        'as' => 'menu.store',
        'middleware' => 'roles',
        'roles' => ['Admin']
    ]);

    // ----------------------------- Deposit ----------------------------

    // deposit Index
    Route::get('/deposit', [
        'uses' => 'DepositController@index',
        'as' => 'deposit.index',
        'middleware' => 'roles',
        'roles' => ['Admin']
    ]);

    // deposit Store
    Route::post('/deposit', [
        'uses' => 'DepositController@store',
        'as' => 'deposit.store',
        'middleware' => 'roles',
        'roles' => ['Admin', 'Owner']
    ]);

    // ----------------------------- Balance ----------------------------

    // balance Index
    Route::get('/balance', [
        'uses' => 'BalanceController@index',
        'as' => 'balance.index',
        'middleware' => 'roles',
        'roles' => ['Admin', 'Owner']
    ]);

    // deposit Store
    Route::post('/balance', [
        'uses' => 'BalanceController@store',
        'as' => 'balance.store',
        'middleware' => 'roles',
        'roles' => ['Admin', 'Owner']
    ]);

    // ----------------------------- Transaction -------------------------

    // transaction Index
    Route::get('/transaction', [
        'uses' => 'TransactionController@index',
        'as' => 'transaction.index',
        'middleware' => 'roles',
        'roles' => ['Cashier', 'Owner']
    ]);

    // transaction Store
    Route::post('/transaction', [
        'uses' => 'TransactionController@store',
        'as' => 'transaction.store',
        'middleware' => 'roles',
        'roles' => ['Cashier', 'Owner']
    ]);

    // ----------------------------- Transaction -------------------------
    // sales index
    Route::get('/sales', [
        'uses' => 'SalesController@index',
        'as' => 'sales.index',
        'middleware' => 'roles',
        'roles' => ['Admin', 'Owner']
    ]);

    // sales Store
    Route::post('/sales', [
        'uses' => 'SalesController@store',
        'as' => 'sales.store',
        'middleware' => 'roles',
        'roles' => ['Admin', 'Owner']
    ]);

    Route::get('/401', function () {
        return view('adminlte::401.index');
    })->name('401');
});
