<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;
use App\Outlet;
class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_admin = Role::where('name', 'Admin')->first();
        $role_cashier = Role::where('name', 'Cashier')->first();
        $role_owner = Role::where('name', 'Owner')->first();

        $outlet_1 = Outlet::where('name', 'KFC')->first();
        $outlet_2 = Outlet::where('name', 'American Warteg')->first();

        $admin = new User();
        $admin->username = 'admin';
        $admin->fullname = 'admiiiin';
        $admin->password = bcrypt('admin');
        $admin->save();
        $admin->outlets()->attach($outlet_1);
        $admin->roles()->attach($role_admin);

        $cashier = new User();
        $cashier->username = 'cashier';
        $cashier->fullname = 'cashieeer';
        $cashier->password = bcrypt('cashier');
        $cashier->save();
        $cashier->outlets()->attach($outlet_1);
        $cashier->roles()->attach($role_cashier);

        $owner = new User();
        $owner->username = 'owner';
        $owner->fullname = 'owneeer';
        $owner->password = bcrypt('owner');
        $owner->save();
        $owner->outlets()->attach($outlet_1);
        $owner->roles()->attach($role_owner);
    }
}
