<?php

use Illuminate\Database\Seeder;
use App\Outlet;

class OutletSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $outlet_1 = new Outlet();
        $outlet_1->name = 'KFC';
        $outlet_1->status = 'Active';
        $outlet_1->save();

        $outlet_2 = new Outlet();
        $outlet_2->name = 'American Warteg';
        $outlet_2->status = 'Inactive';
        $outlet_2->save();

    }
}
