<?php

use Illuminate\Database\Seeder;
use App\Role;
class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $role_admin = new Role();
        $role_admin->name = 'Admin';
        $role_admin->save();

        $role_cashier = new Role();
        $role_cashier->name = 'Cashier';
        $role_cashier->save();

        $role_owner = new Role();
        $role_owner->name = 'Owner';
        $role_owner->save();
    }
}
