<?php

use Illuminate\Database\Seeder;
use App\Customer;

class CustomerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $customer_1 = new Customer();
        $customer_1->customer_group_id = '1';
        $customer_1->card_number = '123456789';
        $customer_1->name = 'rangga';
        $customer_1->email = 'rangga@gmail.com';
        $customer_1->status = 'Active';
        $customer_1->save();

        $customer_2 = new Customer();
        $customer_2->customer_group_id = '2';
        $customer_2->card_number = '987654321';
        $customer_2->name = 'asmin';
        $customer_2->email = 'asmi@gmail.com';
        $customer_2->status = 'Inactive';
        $customer_2->save();
    }
}
