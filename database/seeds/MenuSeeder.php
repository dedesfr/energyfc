<?php

use Illuminate\Database\Seeder;
use App\Menu;

class MenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $menu_1 = new Menu();
        $menu_1->outlet_id = '1';
        $menu_1->type = 'Food';
        $menu_1->name = 'Yam Goreng';
        $menu_1->price = '30000';
        $menu_1->status = 'Active';
        $menu_1->save();

        $menu_2 = new Menu();
        $menu_2->outlet_id = '1';
        $menu_2->type = 'Drink';
        $menu_2->name = 'Es Teh Anget';
        $menu_2->price = '4000';
        $menu_2->status = 'Inactive';
        $menu_2->save();

        $menu_3 = new Menu();
        $menu_3->outlet_id = '2';
        $menu_3->type = 'Food';
        $menu_3->name = 'Do Gado';
        $menu_3->price = '50000';
        $menu_3->status = 'Active';
        $menu_3->save();
    }
}
