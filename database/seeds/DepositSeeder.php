<?php

use Illuminate\Database\Seeder;
use App\Deposit;

class DepositSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $deposit_1 = new Deposit();
        $deposit_1->customer_id = '1';
        $deposit_1->user_id = '1';
        $deposit_1->saldo = '50000';
        $deposit_1->save();

        $deposit_2 = new Deposit();
        $deposit_2->customer_id = '2';
        $deposit_2->user_id = '2';
        $deposit_2->saldo = '3000';
        $deposit_2->save();
    }
}
