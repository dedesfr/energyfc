<?php

use Illuminate\Database\Seeder;
use App\CustomerGroup;

class CustomerGroupSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $customer_group_1 = new CustomerGroup();
        $customer_group_1->name = 'Internal';
        $customer_group_1->save();

        $customer_group_2 = new CustomerGroup();
        $customer_group_2->name = 'External';
        $customer_group_2->save();
    }
}
