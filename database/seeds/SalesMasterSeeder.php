<?php

use Illuminate\Database\Seeder;
use App\SalesMaster as SalesMaster;

class SalesMasterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sales_master = new SalesMaster;
        $sales_master->customer_id = '1';
        $sales_master->user_id = '2';
        $sales_master->total = '30000';
        $sales_master->save();
    }
}
